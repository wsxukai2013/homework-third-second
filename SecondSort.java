package com.xkhadoop.third.second;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
//import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;


public class SecondSort {
	public static class Pair implements WritableComparable<Pair> {
		private LongWritable first;
		private Text second;
		LongWritable s1;
		LongWritable s2;

		public void set(LongWritable left, Text right) {
			first = left;
			second = right;
		}

		public LongWritable getFirst() {
			return first;
		}

		public Text getSecond() {
			return second;
		}

		public void readFields(DataInput in) throws IOException {
			first=new LongWritable(in.readLong());
//			first.readFields(in);
//			second.readFields(in);
			second=new Text(in.readLine());
		}

		public void write(DataOutput out) throws IOException {
			out.writeLong(Long.valueOf(first.toString()));
//			second.write(out);
			out.writeBytes(second.toString());
		}

//		public int compareTo(Pair o) {
//			if (!first.equals(o.getFirst())) {
//				return first.compareTo(o.getFirst());
//			} else if (!second.equals(o.getSecond())) {
//				if (second.toString().startsWith("name"))
//					return -1;
//				else if (o.second.toString().startsWith("name"))
//					return 1;
//				else {
//					s1 = new LongWritable(Long.valueOf(second.toString()));
//					s2 = new LongWritable(Long.valueOf(second.toString()));
//					int i = s1.compareTo(s2);
//					return -i;
//				}
//			}
//			return 0;
//		}
		
		public int compareTo(Pair o) {
			if (!first.equals(o.getFirst())) {
				return first.compareTo(o.getFirst());
			} else
			{
				if (!second.equals(o.getSecond())) 
				{
					if (second.toString().startsWith("name"))
					{
						return -1;
					}
					else if (o.second.toString().startsWith("name"))
					{
						return 1;
					}
					else
					{
						s1 = new LongWritable(Long.valueOf(second.toString()));
						s2 = new LongWritable(Long.valueOf(o.second.toString()));
						int m = s1.compareTo(s2);
						return m;
					}
				}
				return 0;
			}
			
		}

		public int hashCode() {
			return first.hashCode();
		}

		public boolean equals(Pair o) {
			return first.equals(o.getFirst()) && second.equals(o.getSecond());
		}
		
		public String toString()
		{
			return "Pair is:"+" first:"+first.toString()+" second:"+second.toString();
		}
	}
	
	public static class FirstPartitioner extends Partitioner<Pair,Text>{  
        @Override  
        public int getPartition(Pair key, Text value,   
                                int numPartitions) {  
          System.out.println("numPartitions is:"+numPartitions);
          return (int) (Math.abs(key.getFirst().get() * 127) % numPartitions);  
        }  
      }
	
	
	public static class GroupingComparator extends WritableComparator {  
        protected GroupingComparator() {  
          super(Pair.class, true);  
        }  
        @SuppressWarnings("rawtypes")
		@Override  
        public int compare(WritableComparable w1, WritableComparable w2) {  
          Pair ip1 = (Pair) w1;  
          Pair ip2 = (Pair) w2;  
          LongWritable l = ip1.getFirst();  
          LongWritable r = ip2.getFirst();
          return l.compareTo(r);
          //return l == r ? 0 : (l < r ? -1 : 1);  
        }  
      } 
	
	public static class Map extends Mapper<Object, Text, Pair, Text> {

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			FileSplit file1 = (FileSplit) context.getInputSplit();
			String filename = file1.getPath().getName();
			String valueString = value.toString();
			String[] items = valueString.split(" ");
			Pair key1 = new Pair();
			LongWritable outputKey = null;
			Text outputValue = null;
			if (filename.contains("price")) {
				outputKey = new LongWritable(Long.valueOf(items[0].trim()));
				outputValue = new Text(items[1]);
			} else {
				outputKey = new LongWritable(Long.valueOf(items[1].trim()));
				outputValue = new Text("name" + items[0]);
			}
			key1.set(outputKey, outputValue);
			context.write(key1, outputValue);
			System.out.println("Map is : "+" key1:"+key1.toString()+" ****Value:" +outputValue.toString());
		}
	}

	public static class Reduce extends Reducer<Pair, Text, Text, Text> {
		Text itemName = null;
		Text price = null;

		public void reduce(Pair key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			boolean tag = true;
			for (Text val : values) {
				if (tag) {
					itemName = new Text(val.toString().substring(4));
					tag = false;
				} else {
					context.write(itemName, val);
					System.out.println("Reduce is: "+"Name:"+itemName.toString()+" ****Price: "+val.toString());
				}
			}
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		if (otherArgs.length < 2) {
			System.err.println("Usage: wordcount <in> [<in>...] <out>");
			System.exit(2);
		}
		@SuppressWarnings("deprecation")
		Job job = new Job(conf, "SecondSort");
		job.setJarByClass(SecondSort.class);
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setMapOutputKeyClass(Pair.class);
		job.setMapOutputValueClass(Text.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		//job.setNumReduceTasks(3);
		
		 // 分区函数  
        job.setPartitionerClass(FirstPartitioner.class);  
        // 分组函数  
        job.setGroupingComparatorClass(GroupingComparator.class);  
		
		for (int i = 0; i < otherArgs.length - 1; ++i) {
			FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
		}
		FileOutputFormat.setOutputPath(job, new Path(
				otherArgs[otherArgs.length - 1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
